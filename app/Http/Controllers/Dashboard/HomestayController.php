<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Homestay;
use Illuminate\Http\Request;

class HomestayController extends Controller
{
    public function index()
    {
        $homestays = Homestay::paginate();

        return view('pages.homestay.index', compact('homestays'));
    }
}
