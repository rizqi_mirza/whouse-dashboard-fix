<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Homestay;
use App\Models\Reservation;
use App\Models\ReservationHomestay;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request){

        $defaultPeriod = request()->query('daterange') ?? now()->startOfMonth()->format("m/d/Y") . " - " . now()->endOfMonth()->format("m/d/Y");
        [$startDate, $endDate] = explode(" - ", $defaultPeriod);
        $startDate = Carbon::parse($startDate);
        $endDate = Carbon::parse($endDate);

        $homestays = Homestay::all();

        $query = Reservation::whereBetween('date_paid_payment', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()])->get();

        $chart = Reservation::selectRaw('homestay_name, count(channel_name) as total_channel, count(reservations.reservation_id) as amount_reservation, sum(grand_total) as totals, sum(long_day) as total_length_of_stay')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->whereBetween('date_paid_payment', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()])
            ->groupBy('homestay_name')->get();

        $labels = array();

        $data['totals'] = array();

        foreach ($chart as $value) {
            if(!in_array($value->homestay_name,$labels))
                $labels[] = $value->homestay_name;

                $data['totals'][] = $value->totals;

        }

        $chartChannels = Reservation::selectRaw('channel_name, count(channel_name) as total_channel, count(reservations.reservation_id) as amount_reservation, sum(grand_total) as totals, sum(long_day) as total_length_of_stay')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->whereBetween('date_paid_payment', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()])
            ->groupBy('channel_name')->get();

        $labelChannels = array();

        $dataChannels['amount_reservation'] = array();

        foreach ($chartChannels as $value) {
            if(!in_array($value->channel_name,$labels))
                $labelChannels[] = $value->channel_name;

                $data['amount_reservation'][] = $value->amount_reservation;

        }

        return view('pages.dashboard.overview', compact([
            'defaultPeriod', 'homestays', 'query', 'labels', 'data', 'labelChannels', 'dataChannels']));
    }
}
