<?php

namespace App\Http\Controllers\Dashboard\Report\Okupansi;

use App\Exports\HomestayByOkupansiExport;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class HomestayReportByOkupansiController extends Controller
{
    public function index(Request $request)
    {
        $defaultPeriod = request()->query('daterange') ?? now()->startOfMonth()->format("m/d/Y") . " - " . now()->endOfMonth()->format("m/d/Y");
        [$startDate, $endDate] = explode(" - ", $defaultPeriod);
        $startDate = Carbon::parse($startDate);
        $endDate = Carbon::parse($endDate);

        $period = $startDate->diffInDays($endDate);

        $reservations = Reservation::selectRaw('homestay_name, sum(long_day) as total_length_of_stay')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->whereBetween('date_paid_payment', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()])
            ->groupBy('homestay_name')->get();

        $chart = Reservation::selectRaw('homestay_name, sum(long_day) as total_length_of_stay')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->whereBetween('date_paid_payment', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()])
            ->groupBy('homestay_name')->get();

        // dd($reservations);
        $labels = array();

        $data['total_length_of_stay'] = array();

        foreach ($chart as $value) {
            if(!in_array($value->homestay_name,$labels))
                $labels[] = $value->homestay_name;
                $night = $value->total_length_of_stay ;
                $persentase = number_format(($night / $period) *100);
                // dd($night, $period, $persentase);

                $data['total_length_of_stay'][] = $persentase;
        }

        return view('pages.reports.okupansi.homestay', compact('reservations', 'defaultPeriod', 'labels', 'data', 'period'));
    }

    public function export()
    {
        $defaultPeriod = request()->query('daterange') ?? now()->startOfMonth()->format("m/d/Y") . " - " . now()->endOfMonth()->format("m/d/Y");

        [$startDate, $endDate] = explode(" - ", $defaultPeriod);
        $startDate = Carbon::parse($startDate)->format("Y-m-d");
        $endDate = Carbon::parse($endDate)->format("Y-m-d");
        // dd($startDate);
        $filename = "homestay-by-okupansi-$startDate-$endDate.xlsx";

        return (new HomestayByOkupansiExport($startDate, $endDate))->download($filename);

    }
}
