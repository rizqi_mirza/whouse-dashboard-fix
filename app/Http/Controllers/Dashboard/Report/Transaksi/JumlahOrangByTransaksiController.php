<?php

namespace App\Http\Controllers\Dashboard\Report\Transaksi;

use App\Exports\JumlahOrangByTransaksiExport;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JumlahOrangByTransaksiController extends Controller
{
    public function index()
    {
        $defaultPeriod = request()->query('daterange') ?? now()->startOfMonth()->format("m/d/Y") . " - " . now()->endOfMonth()->format("m/d/Y");
        [$startDate, $endDate] = explode(" - ", $defaultPeriod);
        $startDate = Carbon::parse($startDate);
        $endDate = Carbon::parse($endDate);

        $reservations = Reservation::selectRaw('visitor, count(reservations.reservation_id) as amount_reservation, sum(grand_total) as totals,
            sum(long_day) as total_length_of_stay')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->whereBetween('date_paid_payment', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()])
            ->groupBy('visitor')->paginate();

        $chart = Reservation::selectRaw('visitor, count(channel_name) as total_channel, count(reservations.reservation_id) as amount_reservation, sum(grand_total) as totals,
            sum(long_day) as total_length_of_stay')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->whereBetween('date_paid_payment', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()])
            ->groupBy('visitor')->get();

        // dd($reservations);
        $labels = array();

        // $data['total_lodger'] = array();
        $data['amount_reservation'] = array();
        $data['total_length_of_stay'] = array();
        $data['total_channel'] = array();
        $data['totals'] = array();

        foreach ($chart as $value) {
            if(!in_array($value->visitor,$labels))
                $labels[] = $value->visitor . 'Orang';

                // $data['total_lodger'][] = $value->total_lodger;
                $data['amount_reservation'][] = $value->amount_reservation;
                $data['total_length_of_stay'][] = $value->total_length_of_stay;
                $data['total_channel'][] = $value->total_channel;
                $data['totals'][] = $value->totals;

        }

        return view('pages.reports.transaksi.jumlah-orang', compact('reservations', 'defaultPeriod', 'labels', 'data'));
    }

    public function export()
    {
        $defaultPeriod = request()->query('daterange') ?? now()->startOfMonth()->format("m/d/Y") . " - " . now()->endOfMonth()->format("m/d/Y");

        [$startDate, $endDate] = explode(" - ", $defaultPeriod);
        $startDate = Carbon::parse($startDate)->format("Y-m-d");
        $endDate = Carbon::parse($endDate)->format("Y-m-d");
        // dd($startDate);
        $filename = "jumlah-orang-by-transaksi-$startDate-$endDate.xlsx";

        return (new JumlahOrangByTransaksiExport($startDate, $endDate))->download($filename);
    }
}
