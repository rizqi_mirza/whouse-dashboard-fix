<?php

namespace App\Http\Controllers\Dashboard\Report\Transaksi;

use App\Exports\DaerahHomestayByTransaksiExport;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DaerahHHomestayByTransaksiController extends Controller
{
    public function index(Request $request)
    {
        $defaultPeriod = request()->query('daterange') ?? now()->startOfMonth()->format("m/d/Y") . " - " . now()->endOfMonth()->format("m/d/Y");
        [$startDate, $endDate] = explode(" - ", $defaultPeriod);
        $startDate = Carbon::parse($startDate);
        $endDate = Carbon::parse($endDate);

        $reservations = Reservation::selectRaw('subditrict, count(reservations.reservation_id) as amount_reservation,
            sum(long_day) as total_of_length_stay, sum(grand_total) as totals')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->join('homestays', 'reservation_details.homestay_id', '=', 'homestays.homestay_id')
            ->whereBetween('date_paid_payment', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()])
            ->groupBy('subditrict')->paginate();

    // dd($reservations);

        $chart = Reservation::selectRaw('subditrict, count(reservations.reservation_id) as amount_reservation,
            sum(long_day) as total_length_of_stay, sum(grand_total) as totals')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->join('homestays', 'reservation_details.homestay_id', '=', 'homestays.homestay_id')
            ->whereBetween('date_paid_payment', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()])
            ->groupBy('subditrict')->get();
        // dd($chart);
        $labels = array();

        $data['amount_reservation'] = array();
        $data['total_length_of_stay'] = array();
        $data['totals'] = array();


        foreach ($chart as $value) {
            if(!in_array($value->subditrict,$labels))
                $labels[] = $value->subditrict;

                $data['amount_reservation'][] = $value->amount_reservation;
                $data['total_length_of_stay'][] = $value->total_length_of_stay;
                $data['totals'][] = $value->totals;
        }

        // $channels = Channel::get();

        return view('pages.reports.transaksi.daerah-homestay', compact('reservations', 'defaultPeriod', 'labels', 'data'));
    }

    public function export()
    {
        $defaultPeriod = request()->query('daterange') ?? now()->startOfMonth()->format("m/d/Y") . " - " . now()->endOfMonth()->format("m/d/Y");

        [$startDate, $endDate] = explode(" - ", $defaultPeriod);
        $startDate = Carbon::parse($startDate)->format("Y-m-d");
        $endDate = Carbon::parse($endDate)->format("Y-m-d");
        // dd($startDate);
        $filename = "channel-by-transaksi-$startDate-$endDate.xlsx";

        return (new DaerahHomestayByTransaksiExport($startDate, $endDate))->download($filename);
    }
}
