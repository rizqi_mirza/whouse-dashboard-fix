<?php

namespace App\Http\Controllers\Dashboard\Report\Transaksi;

use App\Exports\RoomHomestayByTransaksiExport;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RoomHomestayByTransaksiController extends Controller
{
    public function index()
    {
        $defaultPeriod = request()->query('daterange') ?? now()->startOfMonth()->format("m/d/Y") . " - " . now()->endOfMonth()->format("m/d/Y");
        [$startDate, $endDate] = explode(" - ", $defaultPeriod);
        $startDate = Carbon::parse($startDate);
        $endDate = Carbon::parse($endDate);

        $reservations = Reservation::selectRaw('bed_room, count(reservations.reservation_id) as amount_reservation, sum(grand_total) as totals,
            sum(long_day) as total_length_of_stay, sum(visitor) as total_lodger')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->join('homestays', 'reservation_details.homestay_id', '=', 'homestays.homestay_id')
            ->whereBetween('date_paid_payment', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()])
            ->groupBy('bed_room')->paginate();

        $chart = Reservation::selectRaw('bed_room, count(channel_name) as total_channel, count(reservations.reservation_id) as amount_reservation, sum(grand_total) as totals,
            sum(long_day) as total_length_of_stay, sum(visitor) as total_lodger')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->join('homestays', 'reservation_details.homestay_id', '=', 'homestays.homestay_id')
            ->whereBetween('date_paid_payment', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()])
            ->groupBy('bed_room')->get();

        // dd($reservations);
        $labels = array();

        // $data['total_lodger'] = array();
        $data['amount_reservation'] = array();
        $data['total_length_of_stay'] = array();
        $data['total_channel'] = array();
        $data['totals'] = array();

        foreach ($chart as $value) {
            if(!in_array($value->bed_room,$labels))
                $labels[] = $value->bed_room . ' Kamar';

                // $data['total_lodger'][] = $value->total_lodger;
                $data['amount_reservation'][] = $value->amount_reservation;
                $data['total_length_of_stay'][] = $value->total_length_of_stay;
                $data['total_channel'][] = $value->total_channel;
                $data['totals'][] = $value->totals;

        }

        return view('pages.reports.transaksi.room-homestay', compact('reservations', 'defaultPeriod', 'labels', 'data'));
    }

    public function export()
    {
        $defaultPeriod = request()->query('daterange') ?? now()->startOfMonth()->format("m/d/Y") . " - " . now()->endOfMonth()->format("m/d/Y");

        [$startDate, $endDate] = explode(" - ", $defaultPeriod);
        $startDate = Carbon::parse($startDate)->format("Y-m-d");
        $endDate = Carbon::parse($endDate)->format("Y-m-d");
        // dd($startDate);
        $filename = "room-homestay-by-transaksi-$startDate-$endDate.xlsx";

        return (new RoomHomestayByTransaksiExport($startDate, $endDate))->download($filename);
    }
}
