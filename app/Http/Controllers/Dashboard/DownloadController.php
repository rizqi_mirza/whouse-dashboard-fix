<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{
    public function __invoke(Request $request)
    {
        abort_if(! $request->filled('token'), 404);

        $token = $request->get('token');
        $token = Crypt::decryptString($token);

        if (Storage::exists($token)) {
            try {
                $url = Storage::temporaryUrl(
                    $token,
                    now()->addMinutes(5)
                );
            } catch (\Throwable $th) {
                $url = Storage::url($token);
            }

            return view('pages.export.success', compact('url'));
        }

        return view('pages.export.waiting');
    }
}
