<?php

namespace App\Exports;

use App\Models\Reservation;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class HomestayByTransaksiExport implements FromView, ShouldAutoSize
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct(string $startDate, string $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function view(): View
    {
        $reservations = Reservation::selectRaw('homestay_name, count(reservations.reservation_id) as amount_reservation, sum(grand_total) as totals, sum(long_day) as total_length_of_stay')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->whereDate('date_paid_payment', '>=', $this->startDate)
            ->whereDate('date_paid_payment', '<=', $this->endDate)
            ->groupBy('homestay_name')->get();

        return view('exports.homestay-by-transaksi', compact('reservations'));
    }
}
