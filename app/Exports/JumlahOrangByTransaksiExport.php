<?php

namespace App\Exports;

use App\Models\Reservation;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class JumlahOrangByTransaksiExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;

    public function __construct(string $startDate, string $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function view(): View
    {
        $reservations = Reservation::selectRaw('visitor, count(reservations.reservation_id) as amount_reservation, sum(grand_total) as totals,
            sum(long_day) as total_length_of_stay')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->whereDate('date_paid_payment', '>=', $this->startDate)
            ->whereDate('date_paid_payment', '<=', $this->endDate)
            ->groupBy('visitor')->get();

        return view('exports.jumlah-orang-by-transaksi', compact('reservations'));
    }
}
