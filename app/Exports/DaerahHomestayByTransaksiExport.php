<?php

namespace App\Exports;

use App\Models\Reservation;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DaerahHomestayByTransaksiExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function __construct(string $startDate, string $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function view(): View
    {
        $reservations = Reservation::selectRaw('subditrict, count(reservations.reservation_id) as amount_reservation,
            sum(long_day) as total_of_length_stay, sum(grand_total) as totals')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->join('homestays', 'reservation_details.homestay_id', '=', 'homestays.homestay_id')
            ->whereDate('date_paid_payment', '>=', $this->startDate)
            ->whereDate('date_paid_payment', '<=', $this->endDate)
            ->groupBy('subditrict')->paginate();

        return view('exports.daerah-homestay-by-transaksi', compact('reservations'));
    }
}
