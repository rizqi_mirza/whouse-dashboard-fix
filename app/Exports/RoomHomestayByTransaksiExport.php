<?php

namespace App\Exports;

use App\Models\Reservation;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class RoomHomestayByTransaksiExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function __construct(string $startDate, string $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function view(): View
    {
        $reservations = Reservation::selectRaw('bed_room, count(reservations.reservation_id) as amount_reservation, sum(grand_total) as totals,
            sum(long_day) as total_length_of_stay, sum(visitor) as total_lodger')
            ->join('reservation_details', 'reservations.reservation_id', '=', 'reservation_details.reservation_id')
            ->join('homestays', 'reservation_details.homestay_id', '=', 'homestays.homestay_id')
            ->whereDate('date_paid_payment', '>=', $this->startDate)
            ->whereDate('date_paid_payment', '<=', $this->endDate)
            ->groupBy('bed_room')->get();

        return view('exports.room-homestay-by-transaksi', compact('reservations'));
    }
}
