<table class="table">
    <thead>
        <tr>
            <th>No</th>
            <th scope="col">Homestay</th>
            <th scope="col">Total Malam</th>
            <th scope="col">Persentase Period</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no =1;
        @endphp
        @forelse ($reservations as $reservation)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $reservation->homestay_name }}</td>
            <td>{{ $reservation->total_length_of_stay }}</td>
            <td>{{ number_format(($reservation->total_length_of_stay / $period) * 100) }} %</td>
        </tr>
        @empty
        <tr>
            <td colspan="4" text-align="center">Tidak Ada Data</td>
        </tr>
        @endforelse
    </tbody>
</table>
{{-- @dd() --}}
