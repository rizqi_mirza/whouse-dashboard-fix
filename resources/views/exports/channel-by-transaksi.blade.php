<table class="table datatable">
    <thead>
        <tr>
        <th>#</th>
        <th scope="col">Channel</th>
        <th scope="col">Total Reservasi</th>
        <th scope="col">Total Malam</th>
        <th scope="col">Totals</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no =1;
        @endphp
        @foreach ($reservations as $reservation)
        <tr>
            <td class="align-middle">{{ $no++ }}</td>
            <td>{{ $reservation->channel_name }}</td>
            <td>{{ $reservation->amount_reservation }}</td>
            <td>{{ $reservation->total_length_of_stay}}</td>
            <td>Rp. {{ number_format($reservation->totals) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

