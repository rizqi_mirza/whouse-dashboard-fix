<div>
    <div class="d-md-flex justify-content-between">
        <div class="d-md-flex align-items-center mb-3 mb-md-0">
            <div class="text-left text-muted">
                Menampilkan {{number_format(($paginated->currentPage() -1) * $paginated->count() + 1)}}
                - {{number_format(($paginated->currentPage()-1) * $paginated->count() + $paginated->count())}}
                dari {{number_format($paginated->total())}} total
            </div>
        </div>
        <div class="table-responsive-sm">
            {{ $paginated->onEachSide(1)->appends(request()->input())->links('pagination::bootstrap-4') }}
        </div>
    </div>
</div>
