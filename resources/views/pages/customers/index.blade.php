@extends('layouts.master')
@section('title', 'Orders')

@section('main-content')
<div class="pagetitle">
    <h1>Customer</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item">Customer</li>
      </ol>
    </nav>
</div>

<section class="section">
    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <div class="card-body">
            <br>
            <div class="row mb-3">
                <div class="col-12" id="general">
                    <form action="{{ route('customer.index') }}" method="get" id="formSearch">
                        <div class="row">
                            <div class="col mb-3 mb-md-0">
                                <input type="text" name="search" class="form-control" placeholder="Cari Customer" value="{{ request()->query('search') }}">
                            </div>
                            @if (request()->except('page'))
                                <div class="col-auto">
                                    <a href="{{ route('customer.index') }}" class="btn btn-danger">Reset</a>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
            <!-- Table with stripped rows -->
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Name</th>
                  <th scope="col">Province</th>
                  <th scope="col">Email</th>
                  <th scope="col">Phone</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($customers as $customer)
                    <tr>
                        <td class="align-middle">{{ ($customers->currentPage() - 1) * $customers->perPage() + $loop->iteration }}</td>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->province->name }}</td>
                        <td>{{ $customer->email }}</td>
                        <td>{{ $customer->phone }}</td>
                    </tr>
                @endforeach
              </tbody>
            </table>
            <!-- End Table with stripped rows -->

            <hr>

            <x-pagination :paginated="$customers"></x-pagination>


          </div>
        </div>

      </div>
    </div>
  </section>
@endsection
