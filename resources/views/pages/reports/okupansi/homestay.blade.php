@extends('layouts.master')
@section('title', 'Homestay Report By Okupansi')

@section('page-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('main-content')
<div class="pagetitle">
    <div class="d-flex justify-content-between">
        <div class="breadcrumb">
            <h1>{{ __('Homestay Report By Okupansi') }}</h1>
        </div>
        {{-- start daterangepicker --}}
        <form action="{{ route('reports.okupansi.homestays.index') }}" method="get" id="filterDate" class="col-4">
            <div class="form-row align-items-center">
                <div class="col-auto">
                    <span for="daterange">Filter Date: </span>
                </div>
                <div class="col">
                    <input class="form-control" type="text" id="daterange" name="daterange" value="{{ $defaultPeriod }}" />
                </div>
                @if (request()->except('page'))
                    <div class="col-auto">
                        <a href="{{ route('reports.okupansi.homestays.index') }}" class="btn btn-danger">Reset</a>
                    </div>
                @endif
            </div>
        </form>
        {{-- end daterangepicker --}}
    </div>
</div>

<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Stacked Bar Chart</h5>

                <!-- Stacked Bar Chart -->
                <canvas id="stakedBarChart" style="max-height: 400px;"></canvas>
                <script>
                    document.addEventListener("DOMContentLoaded", () => {
                    new Chart(document.querySelector('#stakedBarChart'), {
                        type: 'bar',
                        data: {
                        labels: @json($labels),
                        datasets: [
                            {
                                label: 'Persentase',
                                data: @json($data['total_length_of_stay']),
                                backgroundColor: [
                                    '#C1232B','#B5C334','#FCCE10','#E87C25'
                                ],
                                stack: 'Stack 0',
                            },
                        ]
                        },
                        options: {
                        plugins: {
                            title: {
                            display: true,
                            text: 'Period Homestay By Okupansi'
                            },
                        },
                        responsive: true,
                        scales: {
                            x: {
                            stacked: true,
                            },
                            y: {
                            stacked: true
                            },
                        },
                        },
                    });
                    });
                </script>
                <!-- End Stacked Bar Chart -->
              </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <a href="{{ route('reports.okupansi.export', request()->query()) }}" class="btn btn-warning">Export</a>
        </div>
        <br>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">

            <div class="card">
            <div class="card-body">
                <br>
                <!-- Table with stripped rows -->

                <table class="table datatable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th scope="col">Homestay</th>
                        <th scope="col">Total Malam</th>
                        <th scope="col">Persentase Period</th>
                    </tr>
                </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @forelse ($reservations as $reservation)
                        <tr>
                            <td class="align-middle">{{ $no++ }}</td>
                            <td>{{ $reservation->homestay_name }}</td>
                            <td>{{ $reservation->total_length_of_stay }}</td>
                            <td>{{ number_format(($reservation->total_length_of_stay / $period) * 100) }} %</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" text-align="center">Tidak Ada Data</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
                <!-- End Table with stripped rows -->
                {{-- <hr> --}}

                {{-- <x-pagination :paginated="$reservations"></x-pagination> --}}

            </div>
            </div>

        </div>
    </div>
</section>
@endsection

@section('page-js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script>
        $(function() {
            $('#daterange').daterangepicker({
                opens: 'left',
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            }).on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                $('#filterDate').submit();
            });
        });
    </script>

    <script>
        $(document).ready(function() {


        })
    </script>
@endsection
