@extends('layouts.master')
@section('title', 'Orders')

@section('main-content')
<div class="pagetitle">
    <h1>Order</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item">Order</li>
      </ol>
    </nav>
</div>

<section class="section">
    <div class="row">
        <div class="col-lg-12">

            <div class="card">
            <div class="card-body">
                <!-- Table with stripped rows -->
                <table class="table datatable">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">No. Reservation</th>
                        <th scope="col">Channel</th>
                        <th scope="col">Nama reservation</th>
                        <th scope="col">Sub Total</th>
                        <th scope="col">Biaya Service</th>
                        <th scope="col">Grand Total</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no =1;
                    @endphp
                    @foreach ($reservations as $reservation)
                    <tr>
                        <td class="align-middle">{{ $no++ }}</td>
                        <td>{{ $reservation->invoice_no }}</td>
                        <td>{{ $reservation->channel_name }}</td>
                        <td>{{ $reservation->customer->name }}</td>
                        <td>Rp. {{ number_format($reservation->totals) }}</td>
                        <td>Rp. {{ number_format($reservation->handling_service) }}</td>
                        <td>Rp. {{ number_format($reservation->grand_total) }}</td>
                        <td>{{ $reservation->status }}</td>
                    </tr>
                    @endforeach
                </tbody>
                </table>

                <hr>

                {{-- <x-pagination :paginated="$reservations"></x-pagination> --}}

            </div>
            </div>

        </div>
    </div>
</section>
@endsection
