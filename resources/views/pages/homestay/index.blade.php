@extends('layouts.master')
@section('title', 'Homestays')

@section('main-content')
<div class="pagetitle">
    <h1>Homestay</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item">Homestay</li>
      </ol>
    </nav>
</div>

<section class="section">
    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <div class="card-body">
            <br>
            {{-- <div class="row mb-3">
                <div class="col-12" id="general">
                    <form action="{{ route('channels.index') }}" method="get" id="formSearch">
                        <div class="row">
                            <div class="col mb-3 mb-md-0">
                                <input type="text" name="search" class="form-control" placeholder="Cari channel" value="{{ request()->query('search') }}">
                            </div>
                            @if (request()->except('page'))
                                <div class="col-auto">
                                    <a href="{{ route('channels.index') }}" class="btn btn-danger">Reset</a>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div> --}}
            <!-- Table with stripped rows -->
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Name</th>
                  <th scope="col">Subdisctrict</th>
                  <th scope="col">Price</th>
                  <th scope="col">Capacity</th>
                  {{-- <th scope="col">Bed Room</th> --}}
                </tr>
              </thead>
              <tbody>
                @foreach ($homestays as $homestay)
                    <tr>
                        <td class="align-middle">{{ ($homestays->currentPage() - 1) * $homestays->perPage() + $loop->iteration }}</td>
                        <td>{{ $homestay->name }}</td>
                        <td>{{ $homestay->subditrict }}</td>
                        <td>{{ $homestay->price }}</td>
                        <td>{{ $homestay->capacity }}</td>
                    </tr>
                @endforeach
              </tbody>
            </table>
            <!-- End Table with stripped rows -->

            <hr>

            <x-pagination :paginated="$homestays"></x-pagination>


          </div>
        </div>

      </div>
    </div>
  </section>
@endsection
