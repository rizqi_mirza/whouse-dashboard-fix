<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

        <li class="nav-item">
            <a class="nav-link " href="{{ route('dashboard.index') }}">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li><!-- End Dashboard Nav -->

        <li class="nav-heading">Report</li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#report-nav" data-bs-toggle="collapse" href="#">
                <i class="bi bi-layout-text-window-reverse"></i><span>Report Occupant</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="report-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                <li>
                    <a href="{{ route('reports.okupansi.homestays.index') }}">
                    <i class="bi bi-circle"></i><span>Homestays</span>
                    </a>
                </li>
            </ul>
        </li><!-- End Charts Nav -->
        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#report-transaction-nav" data-bs-toggle="collapse" href="#">
                <i class="bi bi-layout-text-window-reverse"></i><span>Report Transactions</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="report-transaction-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                <li>
                    <a href="{{ route('reports.transaksi.homestays.index') }}">
                    <i class="bi bi-circle"></i><span>Homestay</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('reports.transaksi.channel.index') }}">
                    <i class="bi bi-circle"></i><span>Channel Pemesanan</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('reports.transaksi.jumlah-orang.index') }}">
                    <i class="bi bi-circle"></i><span>Jumlah Orang</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('reports.transaksi.daerah-homestay.index') }}">
                    <i class="bi bi-circle"></i><span>Daerah Homestay</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('reports.transaksi.asal-customer.index') }}">
                    <i class="bi bi-circle"></i><span>Asal Customer</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('reports.transaksi.room-homestay.index') }}">
                    <i class="bi bi-circle"></i><span>Room Homestay</span>
                    </a>
                </li>
            </ul>
        </li><!-- End Charts Nav -->


        <li class="nav-heading">Customer</li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#customers-nav" data-bs-toggle="collapse" href="#">
                <i class="bi bi-layout-text-window-reverse"></i><span>Customer</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="customers-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                <li>
                    <a href="{{ route('customer.index') }}">
                    <i class="bi bi-circle"></i><span>All Customer</span>
                    </a>
                </li>
            </ul>
        </li><!-- End Charts Nav -->

        <li class="nav-heading">Master Data</li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#homestay-nav" data-bs-toggle="collapse" href="#">
                <i class="bi bi-layout-text-window-reverse"></i><span>Homestay</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="homestay-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                <li>
                    <a href="{{ route('homestays.index') }}">
                    <i class="bi bi-circle"></i><span>All Homestay</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#channel-nav" data-bs-toggle="collapse" href="#">
                <i class="bi bi-layout-text-window-reverse"></i><span>Channel</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="channel-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                <li>
                    <a href="{{ route('channels.index') }}">
                    <i class="bi bi-circle"></i><span>All Channel</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#service-nav" data-bs-toggle="collapse" href="#">
                <i class="bi bi-layout-text-window-reverse"></i><span>Service</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="service-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                <li>
                    <a href="{{ route('services.index') }}">
                    <i class="bi bi-circle"></i><span>All Service</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-heading">Reservation</li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#order-nav" data-bs-toggle="collapse" href="#">
                <i class="bi bi-layout-text-window-reverse"></i><span>Reservations</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="order-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                <li>
                    <a href="{{ route('reservations.index') }}">
                    <i class="bi bi-circle"></i><span>All Reservation</span>
                    </a>
                </li>
            </ul>
        </li>


    </ul>

</aside><!-- End Sidebar-->
