<?php

use App\Http\Controllers\Dashboard\ChannelController;
use App\Http\Controllers\Dashboard\CustomerController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Dashboard\HomestayController;
use App\Http\Controllers\Dashboard\Report\Okupansi\HomestayReportByOkupansiController;
use App\Http\Controllers\Dashboard\Report\Transaksi\AsalCustomerByTransaksiController;
use App\Http\Controllers\Dashboard\Report\Transaksi\ChannelByTransaksiController;
use App\Http\Controllers\Dashboard\Report\Transaksi\DaerahHHomestayByTransaksiController;
use App\Http\Controllers\Dashboard\Report\Transaksi\HomestayByTransaksiController;
use App\Http\Controllers\Dashboard\Report\Transaksi\JumlahOrangByTransaksiController;
use App\Http\Controllers\Dashboard\Report\Transaksi\RoomHomestayByTransaksiController;
use App\Http\Controllers\Dashboard\ReservationController;
use App\Http\Controllers\Dashboard\ServiceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('dashboard.index');
});

Route::prefix('/d')->group(function () {
    Route::resource('dashboard', DashboardController::class)->only('index');

    Route::resource('customer', CustomerController::class)->only('index');

    Route::resource('services', ServiceController::class)->only('index');

    Route::resource('channels', ChannelController::class)->only('index');

    Route::resource('homestays', HomestayController::class)->only('index');

    Route::resource('reservations', ReservationController::class)->only('index');

    Route::prefix('reports')->name('reports.')->group(function() {

        Route::prefix('transaksi')->name('transaksi.')->group(function() {
            Route::get('export-homestay', [HomestayByTransaksiController::class, 'export'])->name('export-homestay');
            Route::get('export-channel', [ChannelByTransaksiController::class, 'export'])->name('export-channel');
            Route::get('export-jumlah-orang', [JumlahOrangByTransaksiController::class, 'export'])->name('export-jumlah-orang');
            Route::get('export-daerah-homestay', [DaerahHHomestayByTransaksiController::class, 'export'])->name('export-daerah-homestay');
            Route::get('export-asal-customer', [AsalCustomerByTransaksiController::class, 'export'])->name('export-asal-customer');
            Route::get('export-room-homestay', [RoomHomestayByTransaksiController::class, 'export'])->name('export-room-homestay');

            Route::resource('homestays', HomestayByTransaksiController::class)->only('index');
            Route::resource('channel', ChannelByTransaksiController::class)->only('index');
            Route::resource('jumlah-orang', JumlahOrangByTransaksiController::class)->only('index');
            Route::resource('daerah-homestay', DaerahHHomestayByTransaksiController::class)->only('index');
            Route::resource('asal-customer', AsalCustomerByTransaksiController::class)->only('index');
            Route::resource('room-homestay', RoomHomestayByTransaksiController::class)->only('index');
        });

        Route::prefix('okupansi')->name('okupansi.')->group(function() {
            Route::get('export', [HomestayReportByOkupansiController::class, 'export'])->name('export');
            Route::resource('homestays', HomestayReportByOkupansiController::class)->only('index');
        });
    });

});
